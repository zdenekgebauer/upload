# Upload

Handle uploaded files

## Features

- upload single or more files
- optionally check MIME type of uploaded file(s) on server side
- optionally check size of uploaded images
- expects PSR7 compatible Request

## Usage

```php
// upload of single file
$request = GuzzleHttp\Psr7\ServerRequest::fromGlobals(); 

$uploader = new \ZdenekGebauer\Upload\Uploader();
// optional settings:
$uploadOptions = new \ZdenekGebauer\Upload\UploadOptions();
// overwrite file with the same name, default false
$uploadOptions->setOverwriteFile(true);  
// set uploaded file permissions, default 0644 
$uploadOptions->setFilePermissions(0666); 
// allow only specified file extensions   
$uploadOptions->setAllowedExtensions(['png', 'jpg', 'jpeg', 'gif']);
// allow only specified MIME types   
$uploadOptions->setAllowedMimeTypes(['image/png', 'image/jpg', 'image/gif']);
// for uploaded images allow max width and/or height. Value 0 means "do not check dimension"
$uploadOptions->setMaxImageSize(1000, 800);
// or $uploadOptions->setMaxImageSize(0, 800); for "any width, max 800px height")     
// or require accurate size 
// $uploadOptions->setRequiredImageSize(640, 480);

try {
    $uploader->save($request->getUploadedFiles()['file'], 'some/dir', $uploadOptions);
    // or
    $uploader->save($request->getUploadedFiles()['file'], 'some/dir/custom-filename', $uploadOptions); 
    echo 'FILE UPLOADED';
} catch (\ZdenekGebauer\Upload\UploadException $exception) {
    echo $exception->getMessage(), ', ', $exception->getCode();
}

// upload all $_FILES 
$uploader = new \ZdenekGebauer\Upload\Uploader();
// optional settings:
$uploadOptions = new \ZdenekGebauer\Upload\UploadOptions();
try {
    $uploader->saveAll($request->getUploadedFiles(), 'some/dir');
    echo 'FILES UPLOADED';
} catch (\ZdenekGebauer\Upload\UploadException $exception) {
    echo $exception->getMessage(), ', ', $exception->getCode();
}
```
