<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Test\Unit;
use DirectoryIterator;
use GuzzleHttp\Psr7\ServerRequest;
use Psr\Http\Message\UploadedFileInterface;
use Tests\Support\UnitTester;
use ZdenekGebauer\Upload\Uploader;
use ZdenekGebauer\Upload\UploadException;
use ZdenekGebauer\Upload\UploadOptions;

class UploaderTest extends Unit
{

    protected UnitTester $tester;

    private string $uploadDir;

    public function testAllowedExtensions(): void
    {
        $file1 = sys_get_temp_dir() . '/test.pdf';
        copy(codecept_data_dir() . '/test.pdf', $file1);
        $_FILES['file']['name'] = basename($file1);
        $_FILES['file']['type'] = 'application/pdf';
        $_FILES['file']['tmp_name'] = $file1;
        $_FILES['file']['error'] = UPLOAD_ERR_OK;
        $_FILES['file']['size'] = filesize($file1);

        $request = ServerRequest::fromGlobals();

        $uploader = new Uploader();
        $options = new UploadOptions();
        $options->setAllowedExtensions(['jpg', 'png']);
        $uploadDir = $this->uploadDir;

        $this->tester->expectThrowable(
            new UploadException('Forbidden extension', UploadException::FORBIDDEN_EXTENSION),
            static function () use ($uploader, $request, $uploadDir, $options) {
                $uploader->save($request->getUploadedFiles()['file'], $uploadDir, $options);
            }
        );
        $this->tester->assertFileNotExists($this->uploadDir . '/test.pdf');
    }

    public function testAllowedMimeTypes(): void
    {
        $file1 = sys_get_temp_dir() . '/test.pdf';
        copy(codecept_data_dir() . '/test_rtf.pdf', $file1);
        $_FILES['file']['name'] = basename($file1);
        $_FILES['file']['type'] = 'application/pdf';
        $_FILES['file']['tmp_name'] = $file1;
        $_FILES['file']['error'] = UPLOAD_ERR_OK;
        $_FILES['file']['size'] = filesize($file1);

        $request = ServerRequest::fromGlobals();

        $uploader = new Uploader();
        $options = new UploadOptions();
        $options->setAllowedMimeTypes(['application/pdf']);
        $uploadDir = $this->uploadDir;

        $this->tester->expectThrowable(
            new UploadException('Forbidden type text/rtf', UploadException::FORBIDDEN_MIME),
            static function () use ($uploader, $request, $uploadDir, $options) {
                $uploader->save($request->getUploadedFiles()['file'], $uploadDir, $options);
            }
        );
        $this->tester->assertFileNotExists($this->uploadDir . '/test.pdf');
    }

    public function testExceptionContainsUploadedFile(): void
    {
        $file1 = sys_get_temp_dir() . '/test.jpg';
        copy(codecept_data_dir() . '/test_rtf.jpg', $file1);
        $_FILES['file']['name'] = basename($file1);
        $_FILES['file']['type'] = 'image/jpeg';
        $_FILES['file']['tmp_name'] = $file1;
        $_FILES['file']['error'] = UPLOAD_ERR_OK;
        $_FILES['file']['size'] = filesize($file1);

        $request = ServerRequest::fromGlobals();

        $uploader = new Uploader();
        $options = new UploadOptions();
        $options->setMaxImageSize(1000, 1000);
        $uploadDir = $this->uploadDir;

        try {
            $uploader->save($request->getUploadedFiles()['file'], $uploadDir, $options);
        } catch (UploadException $exception) {
            $this->tester->assertEquals('Unrecognized image format text/rtf', $exception->getMessage());
            $this->tester->assertInstanceOf(UploadedFileInterface::class, $exception->getUploadedFile());
        }
    }

    public function testImageMaxSize(): void
    {
        $file1 = sys_get_temp_dir() . '/test.jpg';
        copy(codecept_data_dir() . '/test640x480.jpg', $file1);
        $_FILES['file']['name'] = basename($file1);
        $_FILES['file']['type'] = 'image/jpeg';
        $_FILES['file']['tmp_name'] = $file1;
        $_FILES['file']['error'] = UPLOAD_ERR_OK;
        $_FILES['file']['size'] = filesize($file1);

        $request = ServerRequest::fromGlobals();

        $uploader = new Uploader();
        $options = new UploadOptions();
        $options->setMaxImageSize(500, 0);
        $uploadDir = $this->uploadDir;

        $this->tester->expectThrowable(
            new UploadException('Incorrect image size', UploadException::INCORRECT_IMAGE_SIZE),
            static function () use ($uploader, $request, $uploadDir, $options) {
                $uploader->save($request->getUploadedFiles()['file'], $uploadDir, $options);
            }
        );
        $this->tester->assertFileNotExists($this->uploadDir . '/test.jpg');

        $options->setMaxImageSize(1000, 450);
        $this->tester->expectThrowable(
            new UploadException('Incorrect image size', UploadException::INCORRECT_IMAGE_SIZE),
            static function () use ($uploader, $request, $uploadDir, $options) {
                $uploader->save($request->getUploadedFiles()['file'], $uploadDir, $options);
            }
        );
        $this->tester->assertFileNotExists($this->uploadDir . '/test.jpg');

        $options->setMaxImageSize(1000, 490);
        $uploader->save($request->getUploadedFiles()['file'], $uploadDir, $options);

        $this->tester->assertFileExists($this->uploadDir . '/test.jpg');
    }

    public function testImageRequiredSize(): void
    {
        $file1 = sys_get_temp_dir() . '/test.jpg';
        copy(codecept_data_dir() . '/test640x480.jpg', $file1);
        $_FILES['file']['name'] = basename($file1);
        $_FILES['file']['type'] = 'image/jpeg';
        $_FILES['file']['tmp_name'] = $file1;
        $_FILES['file']['error'] = UPLOAD_ERR_OK;
        $_FILES['file']['size'] = filesize($file1);

        $request = ServerRequest::fromGlobals();

        $uploader = new Uploader();
        $options = new UploadOptions();
        $options->setRequiredImageSize(500, 0);
        $uploadDir = $this->uploadDir;

        $this->tester->expectThrowable(
            new UploadException('Incorrect image size', UploadException::INCORRECT_IMAGE_SIZE),
            static function () use ($uploader, $request, $uploadDir, $options) {
                $uploader->save($request->getUploadedFiles()['file'], $uploadDir, $options);
            }
        );
        $this->tester->assertFileNotExists($this->uploadDir . '/test.jpg');

        $options->setRequiredImageSize(0, 450);
        $this->tester->expectThrowable(
            new UploadException('Incorrect image size', UploadException::INCORRECT_IMAGE_SIZE),
            static function () use ($uploader, $request, $uploadDir, $options) {
                $uploader->save($request->getUploadedFiles()['file'], $uploadDir, $options);
            }
        );
        $this->tester->assertFileNotExists($this->uploadDir . '/test.jpg');

        $options->setRequiredImageSize(640, 480);
        $uploader->save($request->getUploadedFiles()['file'], $uploadDir, $options);

        $this->tester->assertFileExists($this->uploadDir . '/test.jpg');
    }

    public function testInvalidImage(): void
    {
        $file1 = sys_get_temp_dir() . '/test.jpg';
        copy(codecept_data_dir() . '/test_rtf.jpg', $file1);
        $_FILES['file']['name'] = basename($file1);
        $_FILES['file']['type'] = 'image/jpeg';
        $_FILES['file']['tmp_name'] = $file1;
        $_FILES['file']['error'] = UPLOAD_ERR_OK;
        $_FILES['file']['size'] = filesize($file1);

        $request = ServerRequest::fromGlobals();

        $uploader = new Uploader();
        $options = new UploadOptions();
        $options->setMaxImageSize(1000, 1000);
        $uploadDir = $this->uploadDir;

        $this->tester->expectThrowable(
            new UploadException('Unrecognized image format text/rtf', UploadException::FORBIDDEN_MIME),
            static function () use ($uploader, $request, $uploadDir, $options) {
                $uploader->save($request->getUploadedFiles()['file'], $uploadDir, $options);
            }
        );
        $this->tester->assertFileNotExists($this->uploadDir . '/test.jpg');
    }

    public function testMaxFileSize(): void
    {
        $file1 = sys_get_temp_dir() . '/test.pdf';
        copy(codecept_data_dir() . '/test.pdf', $file1);
        $_FILES['file']['name'] = basename($file1);
        $_FILES['file']['type'] = 'image/jpeg';
        $_FILES['file']['tmp_name'] = $file1;
        $_FILES['file']['error'] = UPLOAD_ERR_OK;
        $_FILES['file']['size'] = filesize($file1);

        $request = ServerRequest::fromGlobals();

        $uploader = new Uploader();
        $options = new UploadOptions();
        $options->setMaxFileSize(500);
        $uploadDir = $this->uploadDir;

        $this->tester->expectThrowable(
            new UploadException('File too large, max. 500', UploadException::FILE_TOO_LARGE),
            static function () use ($uploader, $request, $uploadDir, $options) {
                $uploader->save($request->getUploadedFiles()['file'], $uploadDir, $options);
            }
        );
        $this->tester->assertFileNotExists($this->uploadDir . '/test.pdf');
    }

    public function testOverwriteFile(): void
    {
        $file1 = sys_get_temp_dir() . '/test.pdf';
        copy(codecept_data_dir() . '/test.pdf', $file1);
        $_FILES['file']['name'] = basename($file1);
        $_FILES['file']['type'] = 'application/pdf';
        $_FILES['file']['tmp_name'] = $file1;
        $_FILES['file']['error'] = UPLOAD_ERR_OK;
        $_FILES['file']['size'] = filesize($file1);
        copy(codecept_data_dir() . '/test2.pdf', $this->uploadDir . '/test.pdf');

        $request = ServerRequest::fromGlobals();

        $uploader = new Uploader();
        $options = new UploadOptions();
        $options->setOverwriteFile(false);
        $uploadDir = $this->uploadDir;

        $this->tester->expectThrowable(
            new UploadException('File already exists:test.pdf', UploadException::FILE_ALREADY_EXISTS),
            static function () use ($uploader, $request, $uploadDir, $options) {
                $uploader->save($request->getUploadedFiles()['file'], $uploadDir, $options);
            }
        );
        $this->tester->assertEquals(
            filesize(codecept_data_dir() . '/test2.pdf'),
            filesize($this->uploadDir . '/test.pdf')
        );

        $options->setOverwriteFile(true);
        $options->setFilePermissions(0666);
        $uploader->save($request->getUploadedFiles()['file'], $uploadDir, $options);
        $this->tester->assertEquals(
            filesize(codecept_data_dir() . '/test.pdf'),
            filesize($this->uploadDir . '/test.pdf')
        );
    }

    public function testUploadFailed(): void
    {
        $file = sys_get_temp_dir() . '/test.pdf';
        copy(codecept_data_dir() . '/test.pdf', $file);
        $_FILES['file']['name'] = basename($file);
        $_FILES['file']['type'] = 'application/pdf';
        $_FILES['file']['tmp_name'] = $file;
        $_FILES['file']['error'] = UPLOAD_ERR_NO_FILE;
        $_FILES['file']['size'] = filesize($file);

        $request = ServerRequest::fromGlobals();

        $uploader = new Uploader();
        $uploadDir = $this->uploadDir;
        $this->tester->expectThrowable(
            new UploadException('No file was uploaded', UPLOAD_ERR_NO_FILE),
            static function () use ($uploader, $request, $uploadDir) {
                $uploader->save($request->getUploadedFiles()['file'], $uploadDir);
            }
        );
        $this->tester->assertFileNotExists($this->uploadDir . '/test.pdf');
    }

    public function testUploadMultipleFiles(): void
    {
        $file1 = sys_get_temp_dir() . '/test.pdf';
        copy(codecept_data_dir() . 'test.pdf', $file1);
        $_FILES['file']['name'] = basename($file1);
        $_FILES['file']['type'] = 'application/pdf';
        $_FILES['file']['tmp_name'] = $file1;
        $_FILES['file']['error'] = UPLOAD_ERR_OK;
        $_FILES['file']['size'] = filesize($file1);

        $file2 = sys_get_temp_dir() . '/test2.pdf';
        copy(codecept_data_dir() . 'test.pdf', $file2);
        $_FILES['multi']['name'][] = basename($file2);
        $_FILES['multi']['type'][] = 'application/pdf';
        $_FILES['multi']['tmp_name'][] = $file2;
        $_FILES['multi']['error'][] = UPLOAD_ERR_OK;
        $_FILES['multi']['size'][] = filesize($file2);

        $file3 = sys_get_temp_dir() . '/test3.pdf';
        copy(codecept_data_dir() . 'test.pdf', $file3);
        $_FILES['multi']['name'][] = basename($file3);
        $_FILES['multi']['type'][] = 'application/pdf';
        $_FILES['multi']['tmp_name'][] = $file3;
        $_FILES['multi']['error'][] = UPLOAD_ERR_OK;
        $_FILES['multi']['size'][] = filesize($file3);

        $file4 = sys_get_temp_dir() . '/test4.jpg';
        copy(codecept_data_dir() . 'test640x480.jpg', $file4);
        $_FILES['multi']['name']['x'] = basename($file4);
        $_FILES['multi']['type']['x'] = 'image/jpeg';
        $_FILES['multi']['tmp_name']['x'] = $file4;
        $_FILES['multi']['error']['x'] = UPLOAD_ERR_OK;
        $_FILES['multi']['size']['x'] = filesize($file4);

        $request = ServerRequest::fromGlobals();

        $uploader = new Uploader();
        $uploader->saveAll($request->getUploadedFiles(), $this->uploadDir);
        $this->tester->assertFileExists($this->uploadDir . '/test.pdf');
        $this->tester->assertFileExists($this->uploadDir . '/test2.pdf');
        $this->tester->assertFileExists($this->uploadDir . '/test3.pdf');
        $this->tester->assertFileExists($this->uploadDir . '/test4.jpg');
    }

    public function testUploadSingleFile(): void
    {
        $file = sys_get_temp_dir() . '/test.pdf';
        copy(codecept_data_dir() . '/test.pdf', $file);
        $_FILES['file']['name'] = basename($file);
        $_FILES['file']['type'] = 'application/pdf';
        $_FILES['file']['tmp_name'] = $file;
        $_FILES['file']['error'] = UPLOAD_ERR_OK;
        $_FILES['file']['size'] = filesize($file);

        $request = ServerRequest::fromGlobals();

        $uploader = new Uploader();
        $uploader->save($request->getUploadedFiles()['file'], $this->uploadDir);
        $this->tester->assertFileExists($this->uploadDir . '/test.pdf');

        unset($_FILES);
        $file = sys_get_temp_dir() . '/test.pdf';
        copy(codecept_data_dir() . '/test.pdf', $file);
        $_FILES['file']['name'] = basename($file);
        $_FILES['file']['type'] = 'application/pdf';
        $_FILES['file']['tmp_name'] = $file;
        $_FILES['file']['error'] = UPLOAD_ERR_OK;
        $_FILES['file']['size'] = filesize($file);
        $request = ServerRequest::fromGlobals();
        $uploader->save($request->getUploadedFiles()['file'], $this->uploadDir . '/custom.pdf');
        $this->tester->assertFileExists($this->uploadDir . '/custom.pdf');
    }

    protected function _after(): void
    {
        foreach (new DirectoryIterator($this->uploadDir) as $fileInfo) {
            if (!$fileInfo->isDot()) {
                unlink($fileInfo->getPathname());
            }
        }
        unset($_FILES);
    }

    protected function _before(): void
    {
        unset($_FILES);
        $this->uploadDir = codecept_data_dir('upload');
    }
}
