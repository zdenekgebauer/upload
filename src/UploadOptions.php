<?php

declare(strict_types=1);

namespace ZdenekGebauer\Upload;

use function in_array;

class UploadOptions
{
    /**
     * @var string[]
     */
    private array $allowedExtensions = [];

    /**
     * @var string[]
     */
    private array $allowedMimeTypes = [];

    private int $imageWidth = 0;

    private int $imageHeight = 0;

    private int $maxImageWidth = 0;

    private int $maxImageHeight = 0;

    private bool $overwriteFile = true;

    private int $filePermissions = 0644;

    private int $maxFileSize = 0;

    public function getFilePermissions(): int
    {
        return $this->filePermissions;
    }

    /**
     * @param int $filePermissions default 0644
     */
    public function setFilePermissions(int $filePermissions): void
    {
        $this->filePermissions = $filePermissions;
    }

    public function getMaxFileSize(): int
    {
        return $this->maxFileSize;
    }

    public function setMaxFileSize(int $maxFileSize): void
    {
        $this->maxFileSize = $maxFileSize;
    }

    public function isExtensionAllowed(string $extension): bool
    {
        return $this->allowedExtensions === [] || in_array($extension, $this->allowedExtensions, true);
    }

    public function isImageRestrictions(): bool
    {
        return $this->maxImageWidth > 0 || $this->maxImageHeight > 0 || $this->imageWidth > 0 || $this->imageHeight > 0;
    }

    public function isImageSizeAllowed(int $width, int $height): bool
    {
        return !(($this->maxImageWidth > 0 && $width > $this->maxImageWidth)
            || ($this->maxImageHeight > 0 && $height > $this->maxImageHeight)
            || ($this->imageWidth > 0 && $width !== $this->imageWidth)
            || ($this->imageHeight > 0 && $height !== $this->imageHeight));
    }

    public function isMimeTypeAllowed(string $mimeType): bool
    {
        return $this->allowedMimeTypes === [] || in_array($mimeType, $this->allowedMimeTypes, true);
    }

    public function isOverwriteFile(): bool
    {
        return $this->overwriteFile;
    }

    /**
     * if $overwriteFile is false and file already exists, upload will throw exception (default is true)
     */
    public function setOverwriteFile(bool $overwriteFile): void
    {
        $this->overwriteFile = $overwriteFile;
    }

    /**
     * @param string[] $allowedExtensions
     */
    public function setAllowedExtensions(array $allowedExtensions): void
    {
        $this->allowedExtensions = array_filter(array_map('trim', $allowedExtensions));
    }

    /**
     * @param string[] $allowedMimeTypes
     */
    public function setAllowedMimeTypes(array $allowedMimeTypes): void
    {
        $this->allowedMimeTypes = array_filter(array_map('trim', $allowedMimeTypes));
    }

    /**
     * dimensions 0 means "ignore"
     */
    public function setMaxImageSize(int $width, int $height): void
    {
        $this->maxImageWidth = $width;
        $this->maxImageHeight = $height;
    }

    /**
     * dimensions 0 means "ignore"
     */
    public function setRequiredImageSize(int $width, int $height): void
    {
        $this->imageWidth = $width;
        $this->imageHeight = $height;
    }
}
