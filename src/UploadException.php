<?php

declare(strict_types=1);

namespace ZdenekGebauer\Upload;

use Exception;
use Psr\Http\Message\UploadedFileInterface;
use Throwable;

class UploadException extends Exception
{
    final public const
        FILE_ALREADY_EXISTS = 1000,
        FORBIDDEN_EXTENSION = 1001,
        FORBIDDEN_MIME = 1002,
        INCORRECT_IMAGE_SIZE = 1003,
        FILE_TOO_LARGE = 1004;

    public function __construct(
        string $message = '',
        int $code = 0,
        Throwable $previous = null,
        private readonly ?UploadedFileInterface $uploadedFile = null
    ) {
        $defaultMessages = [
            UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive',
            UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive',
            UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded',
            UPLOAD_ERR_NO_FILE => 'No file was uploaded',
            UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder',
            UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk',
            UPLOAD_ERR_EXTENSION => 'A PHP extension stopped the file upload',
        ];
        $message = (($message === '' && isset($defaultMessages[$code])) ? $defaultMessages[$code] : $message);

        parent::__construct($message, $code, $previous);
    }

    public function getUploadedFile(): ?UploadedFileInterface
    {
        return $this->uploadedFile;
    }
}
