<?php

declare(strict_types=1);

namespace ZdenekGebauer\Upload;

use finfo;
use Psr\Http\Message\UploadedFileInterface;

use function is_array;
use function is_string;

class Uploader
{
    /**
     * @param array<array<UploadedFileInterface>|UploadedFileInterface> $uploadedFiles
     * @throws UploadException
     */
    public function saveAll(array $uploadedFiles, string $targetDirectory, ?UploadOptions $uploadOptions = null): void
    {
        foreach ($uploadedFiles as $uploadedFile) {
            if ($uploadedFile instanceof UploadedFileInterface) {
                $this->save($uploadedFile, $targetDirectory, $uploadOptions);
            }
            if (is_array($uploadedFile)) {
                $this->saveAll($uploadedFile, $targetDirectory, $uploadOptions);
            }
        }
    }

    /**
     * @throws UploadException
     */
    public function save(
        UploadedFileInterface $uploadedFile,
        string $target,
        ?UploadOptions $uploadOptions = null
    ): void {
        if ($uploadedFile->getError() !== UPLOAD_ERR_OK) {
            throw new UploadException('', $uploadedFile->getError(), null, $uploadedFile);
        }

        $uploadOptions = ($uploadOptions ?: new UploadOptions());
        $this->validate($uploadedFile, $uploadOptions);

        $fullPath = is_dir($target) ? rtrim($target, '/') . '/' . $uploadedFile->getClientFilename() : $target;
        if (is_file($fullPath)) {
            if (!$uploadOptions->isOverwriteFile()) {
                throw new UploadException(
                    'File already exists:' . basename($fullPath),
                    UploadException::FILE_ALREADY_EXISTS,
                    null,
                    $uploadedFile
                );
            }
            unlink($fullPath);
        }

        $uploadedFile->moveTo($fullPath);
        chmod($fullPath, $uploadOptions->getFilePermissions());
    }

    /**
     * @throws UploadException
     */
    private function validate(UploadedFileInterface $uploadFile, UploadOptions $options): void
    {
        if ($options->getMaxFileSize() > 0 && $uploadFile->getSize() > $options->getMaxFileSize()) {
            throw new UploadException(
                'File too large, max. ' . $options->getMaxFileSize(),
                UploadException::FILE_TOO_LARGE,
                null,
                $uploadFile
            );
        }
        $extension = pathinfo((string)$uploadFile->getClientFilename(), PATHINFO_EXTENSION);
        if (!$options->isExtensionAllowed($extension)) {
            throw new UploadException('Forbidden extension', UploadException::FORBIDDEN_EXTENSION, null, $uploadFile);
        }

        $serverMime = $this->getServerMime($uploadFile);
        if (!$options->isMimeTypeAllowed($serverMime)) {
            throw new UploadException(
                'Forbidden type ' . $serverMime,
                UploadException::FORBIDDEN_MIME,
                null,
                $uploadFile
            );
        }

        if (!$options->isImageRestrictions()) {
            return;
        }
        $imgInfo = null;
        $uri = $uploadFile->getStream()->getMetadata('uri');
        if (is_string($uri)) {
            $imgInfo = getimagesize($uri);
        }
        if (!is_array($imgInfo)) {
            throw new UploadException(
                'Unrecognized image format ' . $serverMime,
                UploadException::FORBIDDEN_MIME,
                null,
                $uploadFile
            );
        }
        if (!$options->isImageSizeAllowed($imgInfo[0], $imgInfo[1])) {
            throw new UploadException('Incorrect image size', UploadException::INCORRECT_IMAGE_SIZE, null, $uploadFile);
        }
    }

    private function getServerMime(UploadedFileInterface $uploadedFile): string
    {
        $fileInfo = new finfo(FILEINFO_MIME);
        $uri = $uploadedFile->getStream()->getMetadata('uri');
        $mimeParts = is_string($uri) ? explode(';', (string)($fileInfo->file($uri))) : [''];
        return $mimeParts[0];
    }
}
