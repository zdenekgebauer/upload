<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Set\ValueObject\SetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths(
        [
            __DIR__ . '/tests',
            __DIR__ . '/src',
        ]
    );

    $rectorConfig->skip(
        [
            __DIR__ . '/tests/Support/_generated/*',
            __DIR__ . '/vendor/*',
        ]
    );

    $rectorConfig->disableParallel();

    // register a single rule
//    $rectorConfig->rule(InlineConstructorDefaultToPropertyRector::class);

    // define sets of rules
    $rectorConfig->sets(
        [
            LevelSetList::UP_TO_PHP_82,
            SetList::CODE_QUALITY,
            SetList::DEAD_CODE,
        ]
    );
};
